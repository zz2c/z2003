
import axios from 'axios'
//2. 用axios创建一个axios的实例
const Server = axios.create({
    baseURL: "",//根域名
    timeout: 3000,//超时时间
});
Server.interceptors.request.use((config)=>{
    let token=JSON.parse(localStorage.getItem('zgj_admin'))
    //当进入request拦截器，表示发送了请求，我们就开启进度条
    // config.headers.Authorization=token
    console.log(token)
    config.headers.token=token
    return config
}),(error)=>{

    return Promise.reject(error)
}

// 响应拦截器
Server.interceptors.response.use((response)=>{
     //当进入response拦截器，表示请求已经结束，我们就结束进度条
    return response.data
}),(error)=>{
    return Promise.reject(error)
}

export default Server