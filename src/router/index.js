import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    path: '/',
    name: 'index',
    component: ()=> import('../views/index.vue'),
    children:[
      {
        path: '/sy',
        name: 'sy',
        component: ()=> import('../views/sy.vue'),
      },
      {
        path: '/all',
        name: 'all',
        component: ()=> import('../views/all.vue'),
      },
      {
        path: '/gy',
        name: 'gy',
        component: ()=> import('../views/gy.vue'),
      },
      {
        path: '/gwc',
        name: 'gwc',
        component: ()=> import('../views/gwc.vue'),
      },
      {
        path: '/shoucang',
        name: 'shoucang',
        component: ()=> import('../views/shoucang.vue'),
      },
    ]
  },
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
