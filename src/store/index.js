import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import { stat } from 'fs';
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cat:[],
    num:0,
  },
  mutations: {
    set_cat(state,value){
      console.log(value)
      // state.cat.push(value)
    },
    del(state,value){
      state.cat.splice(value,1)
    },
    set_num(state,num){
    state.num=num
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [vuexLocal.plugin]
})
