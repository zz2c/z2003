import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './util/request'
// axios.defaults.baseURL='https://www.liulongbin.top:8888/api/private/v1/'
Vue.prototype.$axios=axios;
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


// vant配置
import Vant from 'vant';
import 'vant/lib/index.css';




Vue.use(Vant);
Vue.use(ElementUI);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
