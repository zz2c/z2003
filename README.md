# z2003

#### 介绍
购物车案例

#### 软件架构
使用VUE3脚手架创建的项目：用到了vant-ui和element-ui 


#### 安装教程

1.  进入文件夹 cnpm install
2.  npm run serve

#### 使用说明

1.  by:2c
2.  QQ1054711110

#### 参与贡献

1. 前端 2c



#### 图片展示
![首页](https://images.gitee.com/uploads/images/2020/1217/184843_347d3dce_8450528.png "屏幕截图.png")
![收藏](https://images.gitee.com/uploads/images/2020/1217/184925_2872f7ce_8450528.png "屏幕截图.png")
![购物车](https://images.gitee.com/uploads/images/2020/1217/184953_8362fe08_8450528.png "屏幕截图.png")
![分类](https://images.gitee.com/uploads/images/2020/1217/185029_06b9f120_8450528.png "屏幕截图.png")